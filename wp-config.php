<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'magic' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'q!HJtQ_:G(bw8j[6GWt_Z^pQ=Hw_%JwOWEaY h S1?MlM#z<rdI:=X:7kkH:Iqlv' );
define( 'SECURE_AUTH_KEY',  'w9=UlT1RM<og/5q{u1l^Sq j$$B&Za|Muglkc_M4f;QyMDK:!ihML(nTUkr:zrj:' );
define( 'LOGGED_IN_KEY',    'kn9484&|Q!.&^+WxWJc1iF=xg;RX#.yV0cZOpLL0Iz|Vf@7;0zo9nE@~UF-Re>/j' );
define( 'NONCE_KEY',        ' *C4,Mm3R}_+1$)^YrujQ<oj f#Jgn/71q%t !FFqzyTj^=O<D|}~(sRF`H.Nz]1' );
define( 'AUTH_SALT',        'n,,<TM`@[~guKD-0*sz_>w(w@YLBTI1j][|7j&qpKk}Hj&hy!uszBzBQBtON2olR' );
define( 'SECURE_AUTH_SALT', 'cHNIhA`=F7/w7y-kF$%pg(X;}gx5qUy,&Wu>;+Y7+Ge(!inZ%jdzR%xR^<E>uMOT' );
define( 'LOGGED_IN_SALT',   'vmaMD[FY<>?pDr==Q6v84Sqd^6Zuk|41BRgC^$7c[UhZIF30N}7.Y%!CMt6lLdkq' );
define( 'NONCE_SALT',       'VD;}1u5*:5LHd[FlmkT6nevki!Bq>y?k0r@&7K<<$(Kue9_h:NK;l)N/u;L`C~#c' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
