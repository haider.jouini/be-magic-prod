<?php
/**
 * The default template for displaying content
 *
 * Used for both singular and index.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

			<div class="row mt-5 justify-content-center">
				<div class="col-5" style="background-color:;" >
					<div class="row p-3 mt-5">
						<div class="col-12 d-flex">
							<h1 class="mr-auto titre-team-building w-100">TEAM BUILDING</h1>
						</div>
						<div class="col-12 d-flex">
							<h1 class="mr-auto sous_titre-team-building w-100"><?php the_title() ?></h1>
						</div>
						<div class="col-12 d-flex">
							<hr class="hr-team-building m-0 mr-auto " >
						</div>
						<div class="col-12 d-flex team-building-description-top">
							<p class="mr-auto">
							 <?php  the_excerpt(); ?>
							</p>
						</div>
						<div class="col-12 d-flex team-building-description-bot">
							<p class="mr-auto">
							<?php  the_content(); ?>
							</p>
						</div>
						<div class="col-12 d-flex"></div>
					</div>

				</div>
				<div class="col-5 d-flex">
					<div class="m-auto carousel">
						<?php  echo do_shortcode(do_shortcode("[acf field='slider-team-building' post_id='".get_the_ID()."']")); ?>
					</div>
				</div>
			</div>

			<?php

			// echo	get_the_post_thumbnail();

			?>


</article><!-- .post -->
