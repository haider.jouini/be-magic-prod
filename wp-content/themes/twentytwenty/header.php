<?php
/**
 * Header file for the Twenty Twenty WordPress default theme.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

?><!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>>

	<head>

		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >

		<link rel="profile" href="https://gmpg.org/xfn/11">

		<?php wp_head(); ?>
		<!-- <link rel="stylesheet" href="../NewStyles.css"> -->
		<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
		<!-- <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script> -->
		<!-- <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script> -->
		<!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs=" crossorigin="anonymous"></script> -->
		<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> -->
		<script src="https://kit.fontawesome.com/e17f590347.js" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<style>
	
		</style>
	</head>

	<body <?php body_class(); ?>>

		<?php
		wp_body_open();
		?>

	<header id="site-header" class="header-footer-group" role="banner" style="box-shadow: 0px 0px 6px -2px;">
		<div class="top-header" id="top-header">
			<div class="container" id="container">
				<div class="row ">
					<div class="col-7 col-md-7 col-lg-7 col-xl-6 ">
						<div class="row">
							<div class="col-4 col-sm-6 col-md-6 col-lg-4 col-xl-4 item-menu-top">
								<i class="fas fa-phone  mr-2"></i>
									+ 2 0106 5370701
							</div>
							<div class="col-4 col-sm-6 col-md-6 col-lg-4 col-xl-4 item-menu-top">
								<a href="#contact" style="color: unset;text-decoration: none; transition: 0ms!important;" >
									<i class="fas fa-envelope mr-2"></i>
									7oroof@7oroof.com
								</a>
							</div>
						</div>
					</div>
					<div class="col-5 col-md-5 col-lg-5 col-xl-6 ">
						<div class="row justify-content-end">
							<div class="col-6 col-sm-11 col-md-10 col-lg-6 col-xl-6" style="">
								<div class="row justify-content-end">
									<div class="col-2 fb-menu-top" style="border-left: 1px solid #eae9e9;display: flex;padding: 0;">
										<i class="fab fa-facebook-f m-auto"></i>
									</div>
									<div class="col-2 tw-menu-top" style="border-left: 1px solid #eae9e9;display: flex;padding: 0;">
										<i class="fab fa-instagram m-auto"></i>
									</div>
									<div class="col-2 yt-menu-top" style="border-left: 1px solid #eae9e9;display: flex;padding: 0;">
										<i class="fab fa-youtube m-auto"></i>
									</div>
									<div class="col-2 in-menu-top" style="border-left: 1px solid #eae9e9;border-right: 1px solid #eae9e9;display: flex;padding: 0;">
										<i class="fab fa-linkedin-in m-auto"></i>
									</div>
									<div class="col-2" style="">
									&nbsp
										<!-- <i class="fab fa-twitter"></i> -->
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<script>
			$( document ).ready(function() {
				if(screen.width < 900){
					$('#container,#container2').removeClass('container');
				}
				if (screen.width < 610){
					$('#top-header').addClass('d-none');
				}
			});
		</script>
<div class="container" id="container2">
<div class="header-inner section-inner py-3">

	<div class="header-titles-wrapper">

		<?php

		// Check whether the header search is activated in the customizer.
		$enable_header_search = get_theme_mod( 'enable_header_search', true );

		if ( true === $enable_header_search ) {

			?>

			<!-- <button class="toggle search-toggle mobile-search-toggle" data-toggle-target=".search-modal" data-toggle-body-class="showing-search-modal" data-set-focus=".search-modal .search-field" aria-expanded="false">
				<span class="toggle-inner">
					<span class="toggle-icon">
						<?php twentytwenty_the_theme_svg( 'search' ); ?>
					</span>
					<span class="toggle-text"><?php _e( 'Search', 'twentytwenty' ); ?></span>
				</span>
			</button> -->
			<!-- .search-toggle -->

		<?php } ?>

		<div class="header-titles">
			<?php
			// if ($_SERVER['PHP_SELF'] == '/' ) { 
			// 	echo '<img class="w-25" src="./wp-content/uploads/2020/06/logomagic.jpg">';
			// } else  {  
			// 	echo '<img class="w-25" src="../wp-content/uploads/2020/06/logomagic.jpg">';
			// } 
			?>
		<a href="/">	<img class="w-25" src="../wp-content/uploads/2020/06/logomagic.jpg"></a>
		</div><!-- .header-titles -->

		<button class="toggle nav-toggle mobile-nav-toggle mr-5" data-toggle-target=".menu-modal"  data-toggle-body-class="showing-menu-modal" aria-expanded="false" data-set-focus=".close-nav-toggle">
			<span class="toggle-inner">
				<span class="toggle-icon">
					<?php twentytwenty_the_theme_svg( 'ellipsis' ); ?>
				</span>
				<!-- <span class="toggle-text"><?php _e( 'Menu', 'twentytwenty' ); ?></span> -->
			</span>
		</button><!-- .nav-toggle -->

	</div><!-- .header-titles-wrapper -->

	<div class="header-navigation-wrapper">

		<?php
		if ( has_nav_menu( 'primary' ) || ! has_nav_menu( 'expanded' ) ) {
			?>

				<nav class="primary-menu-wrapper" aria-label="<?php esc_attr_e( 'Horizontal', 'twentytwenty' ); ?>" role="navigation">

					<ul class="primary-menu reset-list-style">

					<?php
					if ( has_nav_menu( 'primary' ) ) {

						wp_nav_menu(
							array(
								'container'  => '',
								'items_wrap' => '%3$s',
								'theme_location' => 'primary',
							)
						);

					} elseif ( ! has_nav_menu( 'expanded' ) ) {

						wp_list_pages(
							array(
								'match_menu_classes' => true,
								'show_sub_menu_icons' => true,
								'title_li' => false,
								'walker'   => new TwentyTwenty_Walker_Page(),
							)
						);

					}
					?>

					</ul>

				</nav><!-- .primary-menu-wrapper -->

			<?php
		}

		?>

	</div><!-- .header-navigation-wrapper -->

</div><!-- .header-inner -->

<?php
// Output the search modal (if it is activated in the customizer).
if ( true === $enable_header_search ) {
	get_template_part( 'template-parts/modal-search' );
}
?>

</div>

<?php
// Output the search modal (if it is activated in the customizer).
if ( true === $enable_header_search ) {
get_template_part( 'template-parts/modal-search' );
}
?>

</header><!-- #site-header -->

<?php
// Output the menu modal.
get_template_part( 'template-parts/modal-menu' );
