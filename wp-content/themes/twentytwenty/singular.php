<?php
/**
 * The template for displaying single posts and pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

get_header();
?>

<main id="site-content" role="main">

	<?php

	$cat = get_the_category();
	$categorie = null ;
	foreach ($cat as $key => $value) {
		if ($value->term_id == 5){
			$categorie = $value->term_id ;
		}
	}	
	if ( have_posts() ) {

		while ( have_posts() ) {
			the_post();
			if ($categorie == 5) {
				get_template_part( 'team', get_post_type() );	
			}else {
				get_template_part( 'template-parts/content', get_post_type() );	
			}
		}
	}

	?>

</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>
