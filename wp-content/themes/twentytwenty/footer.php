<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

?>
			<footer id="site-footer" role="contentinfo" class="header-footer-group py-4" style="box-shadow: 0px 1px 10px -4px;" >

				<div class="section-inner">

					<div class="footer-credits">

					</div><!-- .footer-credits -->

					<a class="to-the-top m-auto cadre-arrow-up" href="#site-header">
						<span class="to-the-top-long ">
							<div class="arrow-top ">
								<i class="fas fa-arrow-up " style=""></i>
							</div>
						</span><!-- .to-the-top-long -->
						<span class="to-the-top-short">
							<?php
							/* translators: %s: HTML character for up arrow */
							// printf( __( 'Up %s', 'twentytwenty' ), '<span class="arrow" aria-hidden="true">&uarr;</span>' );
							?>
						</span><!-- .to-the-top-short -->
					</a><!-- .to-the-top -->

				</div><!-- .section-inner -->

			</footer><!-- #site-footer -->

		<?php wp_footer(); ?>

	</body>
</html>
